package com.example.sampleforhin.framework;

/**
 * Created by chrisliu on 7/1/2016.
 */
public interface RespondHandler {

    public void run(Object data);

}
