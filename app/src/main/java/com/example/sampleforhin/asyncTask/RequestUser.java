package com.example.sampleforhin.asyncTask;

import android.os.AsyncTask;
import com.example.sampleforhin.data.User;
import com.example.sampleforhin.framework.RespondHandler;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * A AsyncTask for HTTP request async job
 */
public class RequestUser extends AsyncTask<RespondHandler,String,User>{

    @Override
    protected User doInBackground(RespondHandler... params) {
        RespondHandler handle = params[0];
        HttpGet httppost = new HttpGet("YOU URLS TO JSON");
        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response = null;
        try {
            response = httpclient.execute(httppost);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // StatusLine stat = response.getStatusLine();
        int status = response.getStatusLine().getStatusCode();

        if (status == 200) {
            HttpEntity entity = response.getEntity();
            try {
                String data = EntityUtils.toString(entity);
                User user= new User();
                handle.run(user);
                return user;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
