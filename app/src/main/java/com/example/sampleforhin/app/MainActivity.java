package com.example.sampleforhin.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.example.sampleforhin.asyncTask.RequestUser;
import com.example.sampleforhin.data.User;
import com.example.sampleforhin.framework.RespondHandler;
import com.example.sampleforhin.responsehandler.SimpleRespondHandler;

public class MainActivity extends AppCompatActivity {

    private TextView view = new TextView(getApplicationContext());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Should be onClickHandler() call it
    public void clickHandler(){
        RespondHandler handler = new SimpleRespondHandler(this);
        new RequestUser().execute(handler);
    }

    //Method to updateUI
    //call by SimpleRespondHandler
    public void updateUI(User user){
        view.setText(user.getName());
    }
}
