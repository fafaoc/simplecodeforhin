package com.example.sampleforhin.responsehandler;

import android.app.Activity;
import com.example.sampleforhin.app.MainActivity;
import com.example.sampleforhin.data.User;
import com.example.sampleforhin.framework.RespondHandler;

/**
 * Created by chrisliu on 7/1/2016.
 */
public class SimpleRespondHandler implements RespondHandler {

    MainActivity activity;

    public SimpleRespondHandler(MainActivity activity){
        this.activity =  activity;
    }

    @Override
    public void run(Object data) {
        //Cast use here
        User user = (User) data;
        activity.updateUI(user);
    }
}
